package testbridge;

import testbridge.exceptions.RegistradorInvalidoException;

public class Veiculo {
    protected String placa;
	protected String marca;
	protected String modelo;
	protected Capacidade capacidade;
	protected Carga carga;
	protected float motor;
	protected int nRodas;
    protected Registrador registrador;
	

	protected Veiculo(String placa, String marca, String modelo, Capacidade capacidade, Carga carga, float motor, int nRodas, Registrador registrador) throws RegistradorInvalidoException {
		this.placa = placa;
		this.marca = marca;
		this.modelo = modelo;
		this.capacidade = capacidade;
		this.carga = carga;
		this.motor = motor;
		this.nRodas = nRodas;
        this.SetRegistrador(registrador);
	}
	

	public String GetPlaca() { return this.placa; }

	public String GetMarca() { return this.marca; }

	public String GetModelo() { return this.modelo; }

	public Capacidade GetCapacidade() { return this.capacidade; }

	public Carga GetCarga() { return this.carga; }

	public float GetPotenciaMotor() { return this.motor; }

	public Registrador GetRegistrador() { return this.registrador; }
	public void SetRegistrador(Registrador r) throws RegistradorInvalidoException { 
        if (r.PodeRegistrar(this))
            this.registrador = r; 
        else
            throw new RegistradorInvalidoException(this, r);
    }
    
	public void SaveFichaTecnica() {
        try {
			this.registrador.SaveFichaTecnica(this);
		} catch (RegistradorInvalidoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
