package testbridge.interfacesregistraveis;

public interface RegistravelCSV {
    public String FichaTecnicaFilenameCSV();
    public String[][]FichaTecnicaCSV();
}
