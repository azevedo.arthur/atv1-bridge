package testbridge.interfacesregistraveis;

public interface RegistravelTXT {
    public String FichaTecnicaFilenameTXT();
    public String FichaTecnicaString();
}
