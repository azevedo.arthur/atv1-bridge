package testbridge.exceptions;

import testbridge.Registrador;
import testbridge.Veiculo;

public class RegistradorInvalidoException extends Exception {
    private Veiculo veiculo;
    private Registrador registrador;
    
    public RegistradorInvalidoException(Veiculo v, Registrador r) {
        super("Registrador inválido para tipo de veículo. (Veículo: " + v.getClass().toString() + " | Registrador: " + r.getClass().toString() + ")");
        this.veiculo = v;
        this.registrador = r;
    }

    public Veiculo getVeiculo() {
        return veiculo;
    }
    public Registrador getRegistrador() {
        return registrador;
    }
}
