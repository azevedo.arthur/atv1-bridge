package testbridge.exceptions;

import java.util.ArrayList;

public class VeiculoNotFoundException extends Exception {
    private String procurado;
    private ArrayList<String> registroSnapshot;
    
    public VeiculoNotFoundException(String procurado, ArrayList<String> registroSnapshot) {
        super("Veículo não encontrado no registro atual.\n Procurado: " + procurado + ")\n Registro: " + registroSnapshot.toString());
        this.procurado = procurado;
        this.registroSnapshot = registroSnapshot;
    }

    public String getProcurado() {
        return procurado;
    }
    public ArrayList<String> getRegistroSnapshot() {
        return registroSnapshot;
    }
}
