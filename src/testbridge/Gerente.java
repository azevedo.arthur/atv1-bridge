package testbridge;

import java.util.ArrayList;

import testbridge.exceptions.RegistradorInvalidoException;
import testbridge.exceptions.VeiculoNotFoundException;
import testbridge.registradores.RegistradorCSV;
import testbridge.registradores.RegistradorTXT;
import testbridge.veiculos.Caminhao;
import testbridge.veiculos.Carro;

public class Gerente {
	private String defaultSavepathTXT;
    public String getDefaultSavepathTXT() {
        return this.defaultSavepathTXT;
    }
    public void setDefaultSavepathTXT(String defaultSavepathTXT) {
        this.defaultSavepathTXT = defaultSavepathTXT;
    }
    
	private ArrayList<Veiculo> registroVeiculos;
	
	public Gerente(String defaultSavepathTXT) {
		this.defaultSavepathTXT = defaultSavepathTXT;
		this.registroVeiculos = new ArrayList<Veiculo>();
	}
	

    public ArrayList<String> getRegistroSnapshot() {
        ArrayList<String> snapshot = new ArrayList<>();
        for (Veiculo v : this.registroVeiculos)
            snapshot.add(v.GetPlaca());
        return snapshot;
    }
    public Veiculo getVeiculo(String placa) throws VeiculoNotFoundException {
        for (Veiculo v : this.registroVeiculos) {
            if (v.GetPlaca() == placa) {
                return v;
            }
        }
        throw new VeiculoNotFoundException(placa, this.getRegistroSnapshot());
    }
    
	public void registrarCarro(String placa, String marca, String modelo, Capacidade capacidade, float motor) {
		try {
            this.registroVeiculos.add(new Carro(placa, marca, modelo, capacidade, motor, new RegistradorTXT(this.defaultSavepathTXT)));
        } catch (RegistradorInvalidoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
	public void registrarCaminhao(String placa, String marca, String modelo, Carga carga, float motor, int numeroRodas) {
		try {
            this.registroVeiculos.add(new Caminhao(placa, marca, modelo, carga, motor, numeroRodas, new RegistradorTXT(this.defaultSavepathTXT)));
        } catch (RegistradorInvalidoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}
    public void setRegistradorTXT(String placa, String savepathTXT) throws RegistradorInvalidoException, VeiculoNotFoundException {
        for (Veiculo v : this.registroVeiculos) {
            if (v.GetPlaca() == placa) {
                v.SetRegistrador(new RegistradorTXT(savepathTXT));
                return;
            }
        }
        throw new VeiculoNotFoundException(placa, this.getRegistroSnapshot());
    }
    public void setRegistradorCSV(String placa, String savepathCSV) throws RegistradorInvalidoException, VeiculoNotFoundException {
        for (Veiculo v : this.registroVeiculos) {
            if (v.GetPlaca() == placa) {
                v.SetRegistrador(new RegistradorCSV(savepathCSV));
                return;
            }
        }
        throw new VeiculoNotFoundException(placa, this.getRegistroSnapshot());
    }
    public void removeRegistro(String placa) throws VeiculoNotFoundException {
        for (Veiculo v : this.registroVeiculos) {
            if (v.GetPlaca() == placa) {
                this.registroVeiculos.remove(v);
                return;
            }
        }
        throw new VeiculoNotFoundException(placa, this.getRegistroSnapshot());
    }

	public void saveFichaTecnica(String placa) throws VeiculoNotFoundException {
        for (Veiculo v : this.registroVeiculos) {
            if (v.GetPlaca() == placa) {
                v.SaveFichaTecnica();
                return;
            }
        }
        throw new VeiculoNotFoundException(placa, this.getRegistroSnapshot());
    }
    public void saveFichaTecnicaAll() {
        for (Veiculo v : this.registroVeiculos) {
            v.SaveFichaTecnica();
        }
	}
    
}
