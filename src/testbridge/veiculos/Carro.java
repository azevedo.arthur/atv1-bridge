package testbridge.veiculos;

import testbridge.Capacidade;
import testbridge.Carga;
import testbridge.Registrador;
import testbridge.Veiculo;
import testbridge.exceptions.RegistradorInvalidoException;
import testbridge.interfacesregistraveis.RegistravelCSV;
import testbridge.interfacesregistraveis.RegistravelTXT;

public class Carro extends Veiculo implements RegistravelTXT, RegistravelCSV {
    public Carro(String placa, String marca, String modelo, Capacidade capacidade, float motor, Registrador registrador) throws RegistradorInvalidoException {
        super(placa, marca, modelo, capacidade, Carga.PequenoPorte, motor, 4, registrador);
    }

    @Override
    public String FichaTecnicaFilenameTXT() {
        return "FichaTecnicaCarro(" + this.placa + ")";
    }
    @Override
    public String FichaTecnicaString() {
        String ficha = "Placa: " + this.placa + "\n";
		ficha = ficha + "Marca/Modelo: " + this.marca + " / " + this.modelo + "\n";
		ficha = ficha + "Motor: " + this.motor + "\n";
		ficha = ficha + "Capacidade: " + this.capacidade.toString();
        return ficha;
    }

    @Override
    public String FichaTecnicaFilenameCSV() {
        return "FichaTecnicaCarro(" + this.placa + ")";
    }
    @Override
    public String[][] FichaTecnicaCSV() {
        String[][] ficha = new String[5][2];
        ficha[0][0] = "Placa";
        ficha[0][1] = this.placa;
		ficha[1][0] = "Marca";
        ficha[1][1] = this.marca;
        ficha[2][0] = "Modelo";
        ficha[2][1] = this.modelo;
        ficha[3][0] = "Motor";
        ficha[3][1] = String.valueOf(this.motor);
		ficha[4][0] = "Capacidade";
        ficha[4][1] = this.capacidade.toString();
        return ficha;
    }
}
