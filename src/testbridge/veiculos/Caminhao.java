package testbridge.veiculos;

import testbridge.Capacidade;
import testbridge.Carga;
import testbridge.Registrador;
import testbridge.Veiculo;
import testbridge.exceptions.RegistradorInvalidoException;
import testbridge.interfacesregistraveis.RegistravelCSV;
import testbridge.interfacesregistraveis.RegistravelTXT;

public class Caminhao extends Veiculo implements RegistravelTXT, RegistravelCSV {
    public Caminhao(String placa, String marca, String modelo, Carga carga, float motor, int nRodas, Registrador registrador) throws RegistradorInvalidoException {
        super(placa, marca, modelo, Capacidade.Pequena, carga, motor, nRodas, registrador);
    }

    @Override
    public String FichaTecnicaFilenameTXT() {
        return "FichaTecnicaCaminhao(" + this.placa + ")";
    }
    @Override
    public String FichaTecnicaString() {
        String ficha = "Placa: " + this.placa + "\n";
		ficha = ficha + "Marca/Modelo: " + this.marca + " / " + this.modelo + "\n";
		ficha = ficha + "Motor: " + this.motor + "\n";
		ficha = ficha + "Carga: " + this.carga.toString();
		ficha = ficha + "No. de Rodas: " + this.nRodas;
        return ficha;
    }

    @Override
    public String FichaTecnicaFilenameCSV() {
        return "FichaTecnicaCaminhao(" + this.placa + ")";
    }
    @Override
    public String[][] FichaTecnicaCSV() {
        String[][] ficha = new String[6][2];
        ficha[0][0] = "Placa";
        ficha[0][1] = this.placa;
		ficha[1][0] = "Marca";
        ficha[1][1] = this.marca;
        ficha[2][0] = "Modelo";
        ficha[2][1] = this.modelo;
        ficha[3][0] = "Motor";
        ficha[3][1] = String.valueOf(this.motor);
		ficha[4][0] = "Carga";
        ficha[4][1] = this.carga.toString();
		ficha[5][0] = "No. Rodas";
        ficha[5][1] = String.valueOf(this.nRodas);
        return ficha;
    }
}
