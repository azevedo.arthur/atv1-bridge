package testbridge.registradores;

import java.io.FileWriter;
import java.io.IOException;
import testbridge.Registrador;
import testbridge.interfacesregistraveis.RegistravelTXT;

public class RegistradorTXT extends Registrador {
    protected String savepath;

    public RegistradorTXT(String savepath) {
        this.savepath = savepath;
    }

    public Class<?> GetIntefaceRegistravel() {
        return RegistravelTXT.class;
    }
    protected void SaveFichaTecnicaInternal(Object v) {
        RegistravelTXT vcast = (RegistravelTXT) v;
        try {
	        FileWriter myWriter = new FileWriter(this.savepath + "//" + vcast.FichaTecnicaFilenameTXT() +".txt");
	        myWriter.write(vcast.FichaTecnicaString());
	        myWriter.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
    }
}
