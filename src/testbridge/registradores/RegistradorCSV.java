package testbridge.registradores;

import java.io.FileWriter;
import java.io.IOException;
import testbridge.Registrador;
import testbridge.interfacesregistraveis.RegistravelCSV;

public class RegistradorCSV extends Registrador {
    protected String savepath;

    public RegistradorCSV(String savepath) {
        this.savepath = savepath;
    }

    public Class<?> GetIntefaceRegistravel() {
        return RegistravelCSV.class;
    }
    protected void SaveFichaTecnicaInternal(Object v) {
        RegistravelCSV vcast = (RegistravelCSV) v;
        try {
	        FileWriter myWriter = new FileWriter(this.savepath + "//" + vcast.FichaTecnicaFilenameCSV() +".csv");
            String[][] fichaCSV = vcast.FichaTecnicaCSV();
            String fichaString = "";
            for (String[] line : fichaCSV) {
                for (String row : line)
                    fichaString += row + ",";
                fichaString += "\n";
            }
	        myWriter.write(fichaString);
	        myWriter.close();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
    }
}
