package testbridge;

import testbridge.exceptions.RegistradorInvalidoException;

public abstract class Registrador {
    public abstract Class<?> GetIntefaceRegistravel();
    public Boolean PodeRegistrar(Veiculo v){
        return this.GetIntefaceRegistravel().isInstance(v);
    }
    public void SaveFichaTecnica(Veiculo v) throws RegistradorInvalidoException {
	    if (!PodeRegistrar(v)) throw new RegistradorInvalidoException(v, this);
        this.SaveFichaTecnicaInternal(this.GetIntefaceRegistravel().cast(v));
    }
    protected abstract void SaveFichaTecnicaInternal(Object v);
}
