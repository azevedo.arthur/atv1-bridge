import testbridge.Gerente;
import testbridge.Capacidade;
import testbridge.Carga;

public class App {
    public static void main(String[] args) throws Exception {
        Gerente gerente = new Gerente("C:\\Users\\T-Gamer\\Documents\\SideDrive\\UFMA\\2022.1\\LabProg\\Atv1 (Bridge)\\output");
		
        gerente.registrarCaminhao("PZZ-0923", "Mercedes-Benz", "Caminhao", Carga.GrandePorte, 5.0f, 12);
		gerente.registrarCarro("PTS-1011", "Toyota", "Corolla", Capacidade.Media, 2.0f);
		gerente.registrarCaminhao("AG98P77", "Iveco", "Caminhao2", Carga.MedioPorte, 6.0f, 8);
		gerente.registrarCarro("EXD-1234", "Toyota", "Corolla", Capacidade.Media, 2.0f);

        gerente.setRegistradorCSV("AG98P77", "C:\\Users\\T-Gamer\\Documents\\SideDrive\\UFMA\\2022.1\\LabProg\\Atv1 (Bridge)\\output");
        gerente.setRegistradorCSV("EXD-1234", "C:\\Users\\T-Gamer\\Documents\\SideDrive\\UFMA\\2022.1\\LabProg\\Atv1 (Bridge)\\output");

        gerente.saveFichaTecnicaAll();

        System.out.println("Feito!");
    }
}
